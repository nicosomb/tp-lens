<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // Récupérer la liste des billets actifs
        $posts = $this->get('doctrine')
            ->getRepository('AppBundle:Post')
            ->findByStatus(1, ['createdAt' => 'DESC']);

        // La requête correspondante :
        // SELECT * FROM post where status = 1 order by created_at desc;

        $categories = $this->get('doctrine')
            ->getRepository('AppBundle:Category')
            ->findAll();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $posts,
            $request->query->getInt('page', 1),
            5
        );

        // envoyer à la vue
        return $this->render('default/index.html.twig', [
            'pagination' => $pagination,
            'categories' => $categories,
        ]);
    }
}
