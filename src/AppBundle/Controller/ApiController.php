<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/api")
 */
class ApiController extends FOSRestController
{
    /**
     * Get the Post list.
     *
     * @ApiDoc()
     *
     * @return JsonResponse
     */
    public function getPostsAction()
    {
        $posts = $this->getDoctrine()
            ->getRepository('AppBundle:Post')
            ->findAll();

        $json = $this->get('serializer')->serialize($posts, 'json');

        return (new JsonResponse())->setJson($json);
    }

    /**
     * Get details for one Post.
     *
     * @ApiDoc(
     *      requirements={
     *          {"name"="post", "dataType"="integer", "requirement"="\d+", "description"="The Post ID"}
     *      }
     * )
     *
     * @return JsonResponse
     */
    public function getPostAction(Post $post)
    {
        $json = $this->get('serializer')->serialize($post, 'json');

        return (new JsonResponse())->setJson($json);
    }

    /**
     * Get Category list.
     *
     * @ApiDoc()
     *
     * @return JsonResponse
     */
    public function getCategoriesAction()
    {
        $categories = $this->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->findAll();

        $json = $this->get('serializer')->serialize($categories, 'json');

        return (new JsonResponse())->setJson($json);
    }

    /**
     * Get details for one Category.
     *
     * @ApiDoc(
     *      requirements={
     *          {"name"="category", "dataType"="integer", "requirement"="\d+", "description"="The Category ID"}
     *      }
     * )
     *
     * @return JsonResponse
     */
    public function getCategoryAction(Category $category)
    {
        $json = $this->get('serializer')->serialize($category, 'json');

        return (new JsonResponse())->setJson($json);
    }

    /**
     * Get posts for a given Category.
     *
     * @ApiDoc(
     *      requirements={
     *          {"name"="category", "dataType"="integer", "requirement"="\d+", "description"="The Category ID"}
     *      }
     * )
     *
     * @return JsonResponse
     */
    public function getCategoriesPostsAction(Category $category)
    {
        $json = $this->get('serializer')->serialize($category->getPosts(), 'json');

        return (new JsonResponse())->setJson($json);
    }

    /**
     * Create a new Category.
     *
     * @ApiDoc(
     *       parameters={
     *          {"name"="title", "dataType"="string", "required"=true, "description"="Category title."},
     *       }
     * )
     *
     * @return JsonResponse
     */
    public function postCategoryAction(Request $request)
    {
        $title = $request->request->get('title');

        $category = new Category();
        $category->setTitle($title);

        $em = $this->get('doctrine')->getManager();
        $em->persist($category);
        $em->flush();

        $json = $this->get('serializer')->serialize($category, 'json');

        return (new JsonResponse())->setJson($json);
    }

    /**
     * Create a new Post.
     *
     * @ApiDoc(
     *      requirements={
     *          {"name"="user", "dataType"="integer", "requirement"="\d+", "description"="The User ID"}
     *      },
     *       parameters={
     *          {"name"="title", "dataType"="string", "required"=true, "description"="Post title."},
     *          {"name"="content", "dataType"="textarea", "required"=true, "description"="Post content."},
     *       }
     * )
     *
     * @return JsonResponse
     */
    public function postPostAction(Request $request, User $user)
    {
        $title = $request->request->get('title');
        $content = $request->request->get('content');

        $post = new Post($user);
        $post->setTitle($title);
        $post->setContent($content);
        $post->setStatus(true);

        $em = $this->get('doctrine')->getManager();
        $em->persist($post);
        $em->flush();

        $json = $this->get('serializer')->serialize($post, 'json');

        return (new JsonResponse())->setJson($json);
    }
}
