# Projet

Application de blog

framagit.org/nicosomb/tp-lens/tree/master

## Entités

* Article
	id
	titre
	contenu
	date de création
	date de modification
	status

* Commentaire
    id
    titre
    contenu
    date de création
    date de modification
    status

* Catégorie
    id
    titre

//// NE PAS FAIRE UTILISATEUR ////

* Utilisateur
    id
    nom
    prénom
    email
    nom d'utilisateur
    ...



Un article est écrit par un seul utilisateur.
Un article peut appartenir à une catégorie (une catégorie maximum).
Un article peut avoir des commentaires.
Les commentaires sont saisis de manière libre (pas besoin d'être connecté).


## Todo

* X installer FosUserBundle
* X ajouter bootstrap
* X lien sur titre de billet
* X lister les billets de chaque catégorie
* X Pagination
* X possibilité d'uploader une photo par post
* X relation User / Post
* X commentaires
* X API
* Partage mail 
* Nombre de commentaires sur chaque billet 
* 
