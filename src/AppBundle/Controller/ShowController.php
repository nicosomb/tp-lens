<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use AppBundle\Entity\Category;
use AppBundle\Form\CommentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Show controller.
 *
 * @Route("show")
 */
class ShowController extends Controller
{
    /**
     * @Route("/post/{post}", name="show_post")
     */
    public function showPostAction(Request $request, Post $post)
    {
        $comment = new Comment($post);

        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();

            // On peut bouger ça ensuite côté admin
            $comment->setStatus(true);

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute('show_post', ['post' => $post->getId()]);
        }

        return $this->render('show/post.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/category/{category}", name="show_category")
     */
    public function showCategoryAction(Request $request, Category $category)
    {
        return $this->render('show/category.html.twig', [
            'category' => $category,
        ]);
    }
}
